"""Download data."""

import os
import subprocess
import sys

output = sys.argv[1]

parts = output.rsplit("/", 1)
dir = parts[0]
datafile = parts[1]

# dir = "./project-tree/data/raw-data"
# datafile = "2015-street-tree-census-tree-data.csv"
zipfile = "ny-2015-street-tree-census-tree-data.zip"


os.chdir(dir)

if not os.path.exists(datafile):
    subprocess.run(
        [
            "kaggle",
            "datasets",
            "download",
            "-d",
            "new-york-city/ny-2015-street-tree-census-tree-data",
        ]
    )
    subprocess.run(["7z", "x", zipfile, datafile])
    os.remove(zipfile)

    if os.path.exists(datafile):
        print("Dataset successfully downloaded and unzipped.")
    else:
        print("Error: Dataset file not found.")
else:
    print("Dataset already exists.")
