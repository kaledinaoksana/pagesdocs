import sys

import pandas as pd

input = sys.argv[1]
output = sys.argv[2]
district = sys.argv[3]


df = pd.read_csv(input)
df = df[df["borough"] == district]


def drops_cols(df, columns_to_drop):
    for col in columns_to_drop:
        if col in df.columns:
            df = df.drop(columns=[col])
    return df


# много пропущенных значений
# df = drops_cols(df, ["steward", "guards"])
# в данный момент мы изучаем деревья
df = drops_cols(df, ["block_id", "created_at", "user_type"])
# нет в документации
df = drops_cols(df, ["problems", "council district", "census tract", "bin", "bbl"])
# достаточно lat, lon
df = drops_cols(df, ["x_sp", "y_sp"])
# перевод на латинский колонки spc_common : spc_latin
df = drops_cols(df, ["spc_latin"])
# нам пока не нужно знать сколько человек обследовали дерево + много пропущенных значений
df = drops_cols(df, ["steward"])
# много пропущенных значений
df = drops_cols(df, ["guards"])
# адрес не нужен, используем только lat lon
df = drops_cols(df, ["address"])
# повторяет borocode, только в usa технике
df = drops_cols(df, ["community board"])

# добли в цифрах
df = drops_cols(df, ["borocode", "boro_ct"])
# районы какие-то
# df = drops_cols(df, ["cncldist", "st_assem", "st_senate", "nta"])
df = drops_cols(df, ["state"])

df = df.reset_index(drop=True)

df.to_csv(output, index=False)
