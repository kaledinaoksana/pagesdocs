# Snakefile

# snakemake --sdm conda --cores 4  
# snakemake --dag | dot -Tsvg > dag.svg

districts = ["Manhattan", "Brooklyn"]

rule all: 
    input:
        expand("project-tree/data/test/{district}-sorted.csv", district=districts),
        expand("project-tree/data/pred-data/predicted-{district}.csv", district=districts)
    shell:
        "snakemake --dag | dot -Tsvg > dag_new.svg"

rule sort: 
    input:
        "project-tree/data/pproc-data/cleaned-{district}-tree.csv"
    output:
        "project-tree/data/test/{district}-sorted.csv"
    shell:
        "sort {input} > {output}"

rule model:
    input:
        "project-tree/data/pproc-data/cleaned-{district}-tree.csv"
    output:
        "project-tree/data/pred-data/predicted-{district}.csv"
    conda:
        "envs/env-lin-model.yml"
    shell:
        """
        for di in {districts}; do
            python3 project-tree/models/lin-model.py "project-tree/data/pproc-data/cleaned-$di-tree.csv" \
            "project-tree/data/pred-data/predicted-$di.csv" $di
        done
        """


rule pre_proc:
    input:
        "project-tree/data/raw-data/2015-street-tree-census-tree-data.csv"
    output:
        "project-tree/data/pproc-data/cleaned-{district}-tree.csv"
    conda:
        "envs/env-pre-proc.yml"
    shell:
        """
        for di in {districts}; do
            python3 project-tree/pproc/pproc-dist.py {input} "project-tree/data/pproc-data/cleaned-$di-tree.csv" $di
        done
        """


rule read_data:
    input:
        "project-tree/read-data/read-data-ny-tree-2015.py"
    output:
        "project-tree/data/raw-data/2015-street-tree-census-tree-data.csv"
    conda:
        "envs/env-load-data.yml"
    shell:
        "python3 {input} {output}"
        



